import React, {useState, useEffect} from 'react';
import Axios from 'axios';

const Berbuah = () => {

    // Deklarasi
    const [dataBuah, setDataBuah] = useState(null);
    
    const [input, setInput] = useState({id: null, name: '', price: null, weight: null});

    const [showForm, setShowForm] = useState(false);
    const [callButtonForm, setCallButtonForm] = useState('Tambah');

    const [showUpdate, setShowUpdate] = useState(false);
    const [callButtonEdit, setCallButtonEdit] = useState('Edit');

    // GET API
    useEffect( () => {
        if (dataBuah === null){
            Axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
            .then((result) => {
                setDataBuah(result.data.map(el => {
                    return {
                        id: el.id, 
                        name: el.name,
                        price: el.price,
                        weight: el.weight,
                    };
                }));
            })
        }
    },[dataBuah])

    const submitForm = (event) => {
        event.preventDefault();

        if (input.id === null){
            Axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name: input.name, price: input.price, weight: input.weight})
            .then((result) => {
                let data = result.data;
                setDataBuah([...dataBuah, {
                    id: data.id,
                    name: data.name,
                    price: data.price,
                    weight: data.weight,
                }]);
                setInput({
                    id: null, 
                    name: '', 
                    price: null, 
                    weight: null
                });
            })
        }
    }

    const updateForm = (event) => {
        event.preventDefault();

        Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {name: input.name, price: input.price, weight: input.weight})
        .then((result) => {
            let data = dataBuah.map( el => {
                if (el.id === input.id){
                    el.name = input.name;
                    el.price = input.price;
                    el.weight = input.weight;
                }

                return el ;
            })
            setDataBuah(data);
            setInput({id: null, name: '', price: null, weight: ''});
        })
    }

    const clickDelete = (event) => {
        let id = parseInt(event.target.value);

        Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
        .then((result) => {
            setDataBuah(dataBuah.filter( el => el.id !== id ));
        })
    }

    const clickEdit = (event) => {
        let id = parseInt(event.target.value);

        setInput({ ...input, id: id});
        
        setShowUpdate(!showUpdate);
        setShowForm(false);
        if(showUpdate == false){
            setCallButtonEdit("Cancel");
        }else {
            setCallButtonEdit("Edit");
        }
    }

    const clickTambah = () => {
        setShowForm(!showForm);
        setShowUpdate(false);
        if(showForm == false){
            setCallButtonForm("Sembunyi");
        }else {
            setCallButtonForm("Tambah");
        }
        
    }

    const changeInputName = (event) => {
        setInput({...input, name: event.target.value});
    }

    const changeInputPrice = (event) => {
        setInput({...input, price: event.target.value});
    }

    const changeInputWeight = (event) => {
        setInput({...input, weight: event.target.value});
    }

    return (
        <>
            <h1>Daftar Buah</h1>

            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>AKsi</th>
                    </tr>
                </thead>

                <tbody>
                    {
                        dataBuah !== null && (
                            dataBuah.map( (el, index) => {
                                return(
                                    <>
                                        <tr key={el.id} >
                                            <td> {index +1} </td>
                                            <td> {el.name} </td>
                                            <td> {el.price} </td>
                                            <td> {el.weight} </td>
                                            <td>
                                                <button value={input.id = el.id, input.name= el.name} onClick={clickEdit} > {callButtonEdit} </button>
                                                <button value={el.id} onClick={clickDelete} >Delete</button>
                                            </td>
                                        </tr>
                                    </>
                                );
                            })
                        )
                    }
                    <button onClick={clickTambah} > {callButtonForm} </button>
                    
                </tbody>
            </table>

            {
                showForm && (
                    <>
                    <h1> Tambah Buah </h1>
                    <form onSubmit={submitForm} >
                        <table>
                            <tr>
                                <th>Nama Buah</th>
                                <td> <input type="text" onChange={changeInputName} value={input.name} /> </td>
                            </tr>

                            <tr>
                                <th>Harga Buah</th>
                                <td> <input type="number" onChange={changeInputPrice} value={input.price} /> </td>
                            </tr>

                            <tr>
                                <th>Berat Buah</th>
                                <td> <input type="number" onChange={changeInputWeight} value={input.weight} /> </td>
                            </tr>
                        </table>

                        <button>Simpan</button>
                    </form>
                    </>
                )
            }

            {
                showUpdate && (
                    <>
                    <h1>Update Buah</h1>

                    <form onSubmit={updateForm} >
                        <table>
                            <tr>
                                <th>#ID</th>
                                <td> <input type="text"  value={input.id} /> </td>
                            </tr>
                            
                           <tr>
                               <th>Nama Buah</th>
                               <td> <input type="text" value={input.name} onChange={changeInputName} ></input></td>
                           </tr>

                            <tr>
                                <th>Harga Buah</th>
                                <td> <input type="number" value={input.price} onChange={changeInputPrice} />  </td>
                            </tr>

                            <tr>
                                <th>Berat Buah</th>
                                <td> <input type="number" value={input.weight} onChange={changeInputWeight} />  </td>
                            </tr>
                        </table>

                        <button>Update</button>
                    </form>
                    </>
                )
            }
        </>
    );
}

export default Berbuah;
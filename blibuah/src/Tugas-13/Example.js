import React, {useState, useEffect} from 'react';
import Axios from 'axios';

const Example = () => {
    const [pesertaLomba, setPesertaLomba] = useState(null);
    const [input, setInput] = useState({name: "", id: null});

    useEffect( ()=>{
        if (pesertaLomba === null) {
            Axios.get('http://backendexample.sanbercloud.com/api/contestants')
                .then((result) => {
                    let dataPeserta = result.data;
                    setPesertaLomba(dataPeserta.map( el => {
                        return { id: el.id, name: el.name };
                    }));
                })
        }
    },[pesertaLomba]);

    const submitForm = (event) => {
        event.preventDefault();

        if ( input.id === null ) {
            Axios.post('http://backendexample.sanbercloud.com/api/contestants', {name: input.name})
                .then((result) => {
                    let data = result.data;
                    setPesertaLomba([...pesertaLomba, {id: data.id, name: data.name}]);
                    setInput({id: null, name: ''});
                })
        }else{
            Axios.put(`http://backendexample.sanbercloud.com/api/contestants/${input.id}`, { name: input.name})
            .then(res => {
              var dataPeserta = pesertaLomba.map(x => {
                if (x.id === input.id){
                  x.name = input.name
                }
                return x
              })
              setPesertaLomba(dataPeserta)
              setInput({id: null, name: ""})
            })
        }      
    }

    const handleDelete = (event) => {
        let idPeserta = parseInt(event.target.value);

        Axios.delete(`http://backendexample.sanbercloud.com/api/contestants/${idPeserta}`)
            .then((result) => {
                let dataPeserta = pesertaLomba.filter( el => el.id !== idPeserta)
                setPesertaLomba(dataPeserta);
            })
    }

    const handleEdit = (event) =>{
        var idPeserta= parseInt(event.target.value)
        var peserta = pesertaLomba.find(x=> x.id === idPeserta)
    
        setInput({id: idPeserta, name: peserta.name})
    }    

    const changeInputName = (event) => {
        let value = event.target.value;
        setInput({...input, name: value});
    }

    return (
        <>
            <div>
                <h1>Daftar Peserta Lomba</h1>

                <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            pesertaLomba !== null && (
                                pesertaLomba.map( (item, index) => {
                                    return (
                                        <tr key={item.id} >
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>
                                                <button value={item.id} onClick={handleEdit} >Edit</button>
                                                <button value={item.id} onClick={handleDelete} >Delete</button>
                                            </td>
                                        </tr>
                                    );
                                })
                            )
                        }
                    </tbody>
                </table>
                <br/>
                <br/>
                <form onSubmit={submitForm} >
                    <strong>Nama</strong>
                    <input type="text" value={input.name} onChange={changeInputName} required />
                    <button>Save</button>
                </form>

            </div>
        </>
    );
}


export default Example

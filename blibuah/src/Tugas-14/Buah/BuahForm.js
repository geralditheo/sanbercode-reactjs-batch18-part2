import Axios from 'axios';
import React, { useContext, useState } from 'react';
import { BuahContext } from './BuahContext';

const BuahForm = () => {
    const [buah, setBuah] = useContext(BuahContext);
    const data = useContext(BuahContext);

    const [input, setInput] = useState({id: null, name: '', price: null, weight: null});
    // const [input, setInput] = useContext(BuahContext);
    
    const [showForm, setShowForm] =useState(true);
    const [callButtonForm, setCallButtonForm] = useState('Simpan');

    const [showUpdate, setShowUpdate] = useState(true);
    const [callButtonEdit, setCallButtonEdit] = useState('Edit');

    const handleSubmitForm = (event) => {
        event.preventDefault();

        if (input.id === null){
            Axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name: input.name, price: input.price, weight: input.weight})
            .then((result) => {
                let data = result.data;
                setBuah([...buah, {
                    id: data.id,
                    name: data.name,
                    price: data.price,
                    weight: data.weight,
                }]);

                setInput({
                    id: null,
                    name: '',
                    price: null,
                    weight: null,
                });
            })
        }
    }

    const handleUpdateForm = (event) => {
        event.preventDefault();

        Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${data[2].input}`, {name: input.name, price: input.price, weight: input.weight})
        .then((result) => {
            let data = buah.map( el => {
                if (el.id === input.id){
                    el.name = input.name;
                    el.price = input.price;
                    el.weight = input.weight;
                }

                return el ;
            })
            setBuah(data);
            setInput({id: null, name: '', price: null, weight: ''});

            setShowForm(true);
        })
    }

    const handelChangeInput = (event) => {
        let type = event.target.name;
        let value = event .target.value;

        switch(type){
            case 'name' : {
                setInput({...input, name: value});
            }; break;
            case 'price' : {
                setInput({...input, price: value});
            }; break;
            case 'weight' : {
                setInput({...input, weight: value});
            }; break;
        }
    }

    return (
        <>
            {
                showForm && (
                    <>
                        <h1>Tambah Buah</h1>

                        <form onSubmit={handleSubmitForm} >
                            <table>
                                <tr>
                                    <th>Nama Buah </th>
                                    <td> <input name='name' value={input.name} onChange={handelChangeInput} /> </td>
                                </tr>

                                <tr>
                                    <th>Harga Buah </th>
                                    <td> <input name='price' value={input.price} onChange={handelChangeInput} /> </td>
                                </tr>

                                <tr>
                                    <th>Berat Buah (g)</th>
                                    <td> <input name='weight' value={input.weight} onChange={handelChangeInput} /> </td>
                                </tr>
                            </table>

                            <button> Simpan </button>
                        </form>
                    </>
                )
            }

            {
                showUpdate && (
                    <>
                    <h1>Update Buah</h1>

                    <form onSubmit={handleUpdateForm} >
                        <table>
                            <tr>
                                <th>#ID</th>
                                <td> <input type="text"  value={data[2].input} readOnly /> </td>
                            </tr>
                            
                           <tr>
                               <th>Nama Buah</th>
                               <td> <input type="text" value={input.name} onChange={handelChangeInput} ></input></td>
                           </tr>

                            <tr>
                                <th>Harga Buah</th>
                                <td> <input type="number" value={input.price} onChange={handelChangeInput} />  </td>
                            </tr>

                            <tr>
                                <th>Berat Buah</th>
                                <td> <input type="number" value={input.weight} onChange={handelChangeInput} />  </td>
                            </tr>
                        </table>

                        <button>Update</button>
                    </form>
                    </>
                )
            }
        </>
    );
}

export default BuahForm;
import Axios from 'axios';
import React, { createContext, useEffect, useState } from 'react';

export const BuahContext = createContext();

export const BuahProvider = (props) => {
    const [buah, setBuah] = useState(null);
    const [input, setInput] = useState({input: ''});

    useEffect(()=>{
        if (buah === null){
            Axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
            .then((result) => {
                setBuah( result.data.map( item => {
                    return {
                        id: item.id,
                        name: item.name,
                        price: item.price,
                        weight: item.weight,
                    };
                }));
            })
        }
    }, [buah])

    return (
        <BuahContext.Provider value={[buah, setBuah, input, setInput]}  >
            { props.children }
        </BuahContext.Provider>
    );
}
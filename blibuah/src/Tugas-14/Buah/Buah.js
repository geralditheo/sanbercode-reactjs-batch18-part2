import React from 'react';
import { BuahProvider } from './BuahContext';
import BuahForm from './BuahForm';
import BuahList from './BuahList';

const Buah = () => {


    return (
        <div>
            <h1>Hai</h1>
            <BuahProvider>
                <BuahList></BuahList>
                <BuahForm></BuahForm>
            </BuahProvider>
        </div>
    );
}

export default Buah;
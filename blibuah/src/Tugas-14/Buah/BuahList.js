import React, { createContext, useContext, useState } from 'react';
import { BuahContext } from './BuahContext';
import Axios from 'axios';

const BuahList = () => {
    const [buah, setBuah] = useContext(BuahContext);
    const data = useContext(BuahContext);
    
    const handleClickDelete = (event) => {
        let id = parseInt(event.target.value);

        Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
        .then((result) => {
            setBuah(buah.filter( el => el.id !== id ));
        })
    }

    const handleClickEdit = (event) => {
        let id = parseInt(event.target.value);
        
        data[3]({input: id})
    }

    return (
        <>
            <h1>Daftar Buah { data[2].input} </h1>
            
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Buah</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                {
                    buah !== null && 
                        buah.map( (item, index) => {
                            return(
                                <tr key={item.id} >
                                    <td> {index + 1} </td>
                                    <td> {item.name} </td>
                                    <td> Rp. {item.price} </td>
                                    <td> {item.weight/1000} kg </td>
                                    <td> 
                                        <button value={item.id} onClick={handleClickEdit} >Edit</button>
                                        <button value={item.id} onClick={handleClickDelete} >Delete</button>
                                    </td>
                                </tr>
                            );
                        })
                }
                </tbody>
            </table>
        </>
    );
}

export default BuahList;
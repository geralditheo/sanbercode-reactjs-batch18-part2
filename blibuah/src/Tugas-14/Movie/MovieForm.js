import React, { useContext, useState } from 'react';
import {MovieContext} from './MovieContext';

const MovieForm = () => {
    const [movie, setMovie] = useContext(MovieContext);

    const [name, setName] = useState('');
    const [lengthOfTime, setLengthOfTIme] = useState(0);

    const handleChangeName = (event) => {
        setName(event.target.value);
        
    }

    const handleChangeTime = (event) => {
        setLengthOfTIme(event.target.value);
    }

    const handleSubmitForm = (event) => {
        event.preventDefault();

        setMovie([...movie, {name, lengthOfTime}]);
        setName('');
        setLengthOfTIme('');
    }

    return (
        <>
        <form onSubmit={handleSubmitForm} >
            <input type='text' value={name} onChange={handleChangeName} />
            <input type='number' value={lengthOfTime} onChange={handleChangeTime} />
            <button>Save</button>
        </form>
        </>
    );
}

export default MovieForm;
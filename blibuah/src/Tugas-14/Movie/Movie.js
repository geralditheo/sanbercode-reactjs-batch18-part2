import React from 'react';
import MovieList from './MovieList';
import MovieForm from './MovieForm';
import {MovieProvider} from './MovieContext';

const Movie = () => {
    return (
        <MovieProvider>
            <MovieList></MovieList>
            <MovieForm></MovieForm>
        </MovieProvider>
    );
};

export default Movie;
import React, { useContext } from 'react';
import { MovieContext } from './MovieContext';

const MovieList = () => {
    const [movie] = useContext(MovieContext);

    return(
        <ul>
            {
                movie.map( (element) => {
                    return (
                        <li> name: {element.name} {element.lengthOfTime} minutes </li>
                    );
                })
            }
        </ul>
    );
}

export default MovieList;
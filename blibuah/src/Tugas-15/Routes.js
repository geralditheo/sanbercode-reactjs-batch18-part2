import React from "react";
import {Switch, Route, Link } from "react-router-dom";
import Nav from '../Tugas-15/Nav'

import FormBuah from '../Tugas-9/FormBuah.js';
import TableBuah from '../Tugas-10/TableBuah.js'
import Timer from '../Tugas-11/Timer.js';
import ListFormBuah from '../Tugas-12/ListFormBuah.js';
import Berbuah from '../Tugas-13/Berbuah.js'
import Example from '../Tugas-13/Example'
import Movie from '../Tugas-14/Movie/Movie.js';
import Buah from '../Tugas-14/Buah/Buah.js';

const Routes = () => {

  return (
    
      <div  >
        
        {/* <Nav></Nav> */}

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
            <Route path="/home">
                <Home />
            </Route>
            <Route path="/formbuah" >
                <FormBuah></FormBuah>
            </Route>
            <Route path="/tablebuah" >
                <TableBuah></TableBuah>
            </Route>
            <Route path="/timer" >
                <Timer></Timer>
            </Route>
            <Route path="/listbuah" >
                <ListFormBuah></ListFormBuah>
            </Route>
            <Route path="/berbuah" >
                <Berbuah></Berbuah>
            </Route>
            <Route path="/example" >
                <Example></Example>
            </Route>
            <Route path="/buah" >
                <Buah></Buah>
            </Route>
            <Route path="/movie" >
                <Movie></Movie>
            </Route>
        </Switch>
      </div>
    
  );
};

function Home() {
    return <h2>Home</h2>;
}
  

export default Routes;
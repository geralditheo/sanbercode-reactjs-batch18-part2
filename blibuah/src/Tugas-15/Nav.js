import React from "react";
import {Link} from "react-router-dom";
import './style/NavCss.css';

const Nav = () => {



    return (
        <>
        <nav className='NavigationBar' >
            <ul>
                <li>
                    <a><Link to="/home">Home</Link></a>
                </li>
                <li>
                    <a><Link to="/formbuah">Form Buah</Link></a>
                </li>
                <li>
                    <a><Link to="/tablebuah">Tabel Buah</Link></a>
                </li>
                <li>
                    <a><Link to='/timer' >Timer</Link></a>
                </li>
                <li>
                    <a><Link to='/listbuah' >List Buah</Link></a>
                </li>
                <li>
                    <a><Link to='/berbuah' >Berbuah</Link></a>
                </li>
                <li>
                    <a><Link to='/example' >Example</Link></a>
                </li>
                <li>
                    <a><Link to='/buah' >Buah</Link></a>
                </li>
                <li>
                    <a><Link to='/movie' >Movie</Link></a>
                </li>
                <li>
                    <label className='togggleswitch' >
                        <input type='checkbox'/>
                        <span className='slider'/>
                    </label>
                </li>


            </ul>

            </nav>
        </>
    )
}

export default Nav
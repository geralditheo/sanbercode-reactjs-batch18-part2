import React from 'react';
import {ThemeContext} from './ThemeContext';

import Nav from '../Nav';
import Routes from '../Routes'
import { BrowserRouter as Router} from "react-router-dom";

class ThemedButton extends React.Component {
  render() {
    let props = this.props;
    let theme = this.context;
    return (
      <>
        <Router>
          <Nav />
          <div
            style={{
              backgroundColor: theme.background,
              width: '100%',
              color: theme.text,
            }}
            >  
            
            <Routes />
          </div>
          <div {...props} >
            <button>Change Background</button>
          </div>
        </Router>
      </>
      
    );
  }
}

ThemedButton.contextType = ThemeContext;

export default ThemedButton;
import React from 'react';
import './App.css';

import Routes from './Tugas-15/Routes.js'
import Nav from './Tugas-15/Nav.js';
import { BrowserRouter as Router} from "react-router-dom";

import Theme from './Tugas-15/NavTheme/Theme';


function App() {

  

  return (
    <div>
      
      {/* <Router>
        <Nav />
        <Routes />
      </Router> */}
      

      <Theme />
    </div>
  );
}

export default App;

import React from 'react';

class Timer extends React.Component
{
    constructor(props)
    {
        super(props);
        let date = new Date();

        this.state = 
        {
            time: 100,
            ampm: 0,
            
            hours: date.getHours(),
            shour: 0,

            minutes: date.getMinutes(),
            seconds: date.getSeconds(),
            show: true,
        }
    }

    componentDidMount()
    {
        console.log("Did");
        this._timerID = setInterval(
            () => 
            { 
                this.countDown();
                this.timeDate();
                
            }
        ,1000);
    }

    componentDidUpdate()
    {
        if (this.state.time == 0)
        {
            this.componentWillUnmount();
        }
    }

    componentWillUnmount()
    {   
        clearInterval(this._timerID);
        this.state.show = false;
    }

    countDown()
    {
        this.setState({
            time: this.state.time - 1,
        });
    }

    timeDate()
    {
        let date = new Date();
        this.setState(
            {
                minutes: date.getMinutes(),
                seconds: date.getSeconds(),
                
                hours: date.getHours(),
                shour: this.state.hours % 12,
                
                ampm: this.state.hours >= 12 ? 'PM' : 'AM'
            }
        )
    }

    render()
    {
        return (
            <>
            {
                this.state.show && (
                    <div>
                        <h1 style={{ position: "absolute", left: "100px" }}
                        >Sekarang Jam {this.state.shour} : {this.state.minutes} : {this.state.seconds} {this.state.ampm} </h1>
                        
                        <h1 style={{ position:"absolute", textAlign: "right", right: "100px" }} 
                        >Hitung Mundur {this.state.time} </h1>
                    </div>
                )
            }
            </>
        );
    }
}

export default Timer;
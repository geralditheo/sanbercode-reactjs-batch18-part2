import React from 'react';

class ListFormBuah extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            dataBuah: [
                {name: "Semangka", price: 10000, weight: 1000},
                {name: "Anggur", price: 40000, weight: 500},
                {name: "Strawberry", price: 30000, weight: 400},
                {name: "Jeruk", price: 30000, weight: 1000},
                {name: "Mangga", price: 30000, weight: 500},
            ],
            inputName: '',
            inputPrice: '',
            inputWeight: '',

            showForm: false,
            callbutton: 'show',

            callbuttonedit: 'edit',
            showUpdate: false,
            updateID: 0,

        }
    }

    // Buka Tutup Form
    showhide = () =>
    {
        this.state.showForm == false ? this.state.callbutton = "Hide" : this.state.callbutton = "show";
        this.setState( { showForm: !this.state.showForm } );
    }


    // Handle
    handleSubmit = (event) =>
    {
        event.preventDefault();
        this.setState({
            dataBuah: [
                ...this.state.dataBuah, 
                {name: this.state.inputName, 
                price: this.state.inputPrice, 
                weight: this.state.inputWeight}
            ],
            inputName: '',
            inputPrice: '',
            inputWeight: '',
        });
    }

    handleChangeName = (event) => 
    {
        this.setState({
            inputName: event.target.value,
        });
    }
    handleChangePrice = (event) => 
    {
        this.setState({
            inputPrice: event.target.value,
        });
    }
    handleChangeweight = (event) => 
    {
        this.setState({
            inputWeight: event.target.value,
        });
    }

    handleDelete = (event) =>
    {
        this._index = event.target.value;
        this.state.dataBuah.splice(this._index,1);
        this.setState({
            dataBuah: [...this.state.dataBuah]
        })
    }

    handleEdit = (event) => 
    {
        this.state.showUpdate == false ? this.state.callbuttonedit = "cancel" : this.state.callbuttonedit = "edit"
        this.setState( { showUpdate: !this.state.showUpdate } );
        this.setState({
            updateID: event.target.value,
        });
    }

    handleUpdate = (event) => 
    {
        event.preventDefault();
        this._index = this.state.updateID;
        this.state.dataBuah.map( (el, index) => {
            if (index == this._index)
            {
                console.log("Updated");
                console.log(this._index);
                return (
                    <>
                        {el.name = this.state.inputName},
                        {el.price = this.state.inputPrice  },
                        {el.weight = this.state.inputWeight },
                    </>
                );
            }
        })

        this.setState({
            showUpdate: false,
            inputName: '',
            inputPrice: '',
            inputWeight: '',
            callbuttonedit: 'edit',
        })

        this.setState({
            dataBuah: [...this.state.dataBuah]
        })
    }

    render()
    {
        const stylus = 
        {
            width: "50%", 
            margin: "0 auto",
            marginTop: '100px',
            fontFamily: "serif",
        }

        const tabel = 
        {
            border: '1px solid',
            width: "100%", 
            margin: "0 auto",
        }

        return(
            <div style={stylus} >
                <h1 style={{  textAlign: "center"} } >Tabel Harga Buah</h1>

                <table style={tabel} >
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.dataBuah.map((el, index)=>{
                                return (
                                    <tr>
                                        <td> {index + 1} </td>
                                        <td> {el.name} </td>
                                        <td> {el.price} </td>
                                        <td> {el.weight/1000} kg </td>
                                        <td style={{  textAlign: "center"} } >
                                            <button value={index} onClick={ this.handleEdit } > { this.state.callbuttonedit } </button>
                                            <button value={index} onClick={ this.handleDelete } >Delete</button>
                                        </td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </table>

                <button onClick={this.showhide}  >{ this.state.callbutton }</button>

                {
                    this.state.showForm && (
                        <form onSubmit={this.handleSubmit}>
                            <h1 style={{  textAlign: "center"} } >Form Buah</h1>
                            <table>

                                <tr>
                                    <td>
                                        <label>Nama Buah</label>
                                    </td>
                                    <td>
                                        <input type="text" value={this.state.inputName}  onChange={this.handleChangeName} />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>Harga Buah</label>
                                    </td>
                                    <td>
                                        <input type="number"  value={this.state.inputPrice} onChange={this.handleChangePrice} />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>Berat Buah</label>
                                    </td>
                                    <td>
                                        <input type="number" value={this.state.inputWeight} onChange={this.handleChangeweight}  />
                                    </td>
                                </tr>
                            </table>

                            <input type='submit' value="submit" />
                        </form>
                    )
                }

                {
                    this.state.showUpdate && ( 
                        <>
                        <h1 style={{  textAlign: "center"} } >Update Buah</h1>
                        <form onSubmit={ this.handleUpdate }  >
                            <table  >

                                <tr>
                                    <td>
                                        <label>ID #</label>
                                    </td>
                                    <td>
                                        <input type="number" value={ parseInt(this.state.updateID) + 1 } readOnly />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>Nama Buah</label>
                                    </td>
                                    <td>
                                        <input type="text" value={this.state.inputname} onChange={this.handleChangeName}  />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>Harga Buah</label>
                                    </td>
                                    <td>
                                        <input type="number"  value={this.state.inputPrice} onChange={ this.handleChangePrice } />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>Berat Buah</label>
                                    </td>
                                    <td>
                                        <input type="number" value={this.state.inputWeight} onChange={ this.handleChangeweight }  />
                                    </td>
                                </tr>
                            </table>

                            <input type='submit' value="update" />
                        </form>
                        </>
                     )
                }
            </div>
        );
    }
}

export default ListFormBuah;
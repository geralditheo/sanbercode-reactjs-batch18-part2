import React from 'react';
import '../App.css';

class Table extends React.Component
{
  render ()
  {
    const tabledata = 
    {
      margin: "10px",
      backgroundColor: "#fdc879",
      
    }

    return (
      <> 
        <tr>
          <td style={tabledata}> {this.props.name} </td>
          <td style={tabledata}> {this.props.price} </td>
          <td style={tabledata}> {this.props.weight} kg </td>
        </tr>
      </>
    );
  }
}

let buah = [
  {name: "Semangka", price: 10000, weight: 1000},
  {name: "Anggur", price: 40000, weight: 500},
  {name: "Strawberry", price: 30000, weight: 400},
  {name: "Jeruk", price: 30000, weight: 1000},
  {name: "Mangga", price: 30000, weight: 500},
]

class TableBuah extends React.Component {
  render() {

    const csstable = 
    {
      border: "1px solid",
      padding: "1px",
      width: "50%", 
      margin: "0 auto",
      fontFamily: "serif"
    }

    const tablehead = 
    {
      width: "300px",
      textAlign: "center",
      backgroundColor: "grey",
      fontFamily: "serif"
    }

    return (
      <div>
        <h1 style={{textAlign:"center", fontFamily: "serif"}} >Tabel Harga Buah </h1>

        <table style={csstable} >
          
          <tr>
            <th style={tablehead} >Name</th>
            <th style={tablehead} >Harga</th>
            <th style={tablehead} >Berat</th>
          </tr>

          {
            buah.map( (el) =>  {
              return (
                <Table name={el.name} price={el.price} weight={el.weight/1000} />
              );
            })
          }
          
        </table>
      </div>
    );
  }
}

export default TableBuah;